<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mahasiswaModel extends Model
{
    protected $table = 'mahasiswa';
    public $timestamps = false;
    public $primaryKey = 'student_id';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assignmentModel extends Model
{
    protected $table = 'assignment';
    public $timestamps = false;
    public $primaryKey = 'assignment_id';
}

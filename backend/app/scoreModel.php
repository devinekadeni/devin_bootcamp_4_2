<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scoreModel extends Model
{
    protected $table = 'score';
    public $timestamps = false;
}

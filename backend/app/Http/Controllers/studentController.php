<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mahasiswaModel;
use Illuminate\Support\Facades\DB;

class studentController extends Controller
{
    function add_edit_student(Request $request){
        $name = $request->input('name');
        $classroom = $request->input('classroom');
        $grade = $request->input('grade');
        $id = $request->input('student_id');

        DB::beginTransaction();
        try{
            $this->validate($request,[
                'name' => 'required',
                'classroom' => 'required',
                'grade' => 'required'
            ]);

            $temp = mahasiswaModel::updateOrCreate(
                ['student_id'=>$id],
                ['name'=>$name, 'classroom'=>$classroom, 'grade'=>$grade]
            );

            DB::commit();
            return response()->json($temp,201);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json($e->get_message(),500);
        }
    }
}

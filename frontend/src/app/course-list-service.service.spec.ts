import { TestBed, inject } from '@angular/core/testing';

import { CourseListServiceService } from './course-list-service.service';

describe('CourseListServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CourseListServiceService]
    });
  });

  it('should be created', inject([CourseListServiceService], (service: CourseListServiceService) => {
    expect(service).toBeTruthy();
  }));
});

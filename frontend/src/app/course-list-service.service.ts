import { Injectable } from '@angular/core';

@Injectable()
export class CourseListServiceService {

  constructor() { }

  courselist:Object[] = [
    {"id":1, "name":"Programming Fundamental", "hour":3, "capacity":30, "start":"08:00:00", "end":"11:00:00"},
    {"id":2, "name":"Database SQL", "hour":4, "capacity":30, "start":"09:00:00", "end":"14:00:00"},
    {"id":3, "name":"Front End Development", "hour":8, "capacity":40, "start":"08:00:00", "end":"17:00:00"},
    {"id":4, "name":"Back End Development", "hour":8, "capacity":40, "start":"08:00:00", "end":"17:00:00"}
  ]

}

import { Component, OnInit } from '@angular/core';
import { CourseListServiceService } from '../course-list-service.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  constructor(private api:CourseListServiceService) { }

  ngOnInit() {
  }

  bName:string = "";
  bHour:number = null;
  bCapacity:number = null;
  bStart:string = "";
  bEnd:string = "";

  addNew(){
    let newID = this.api.courselist[this.api.courselist.length-1]["id"] + 1;
    this.api.courselist.push({"id":newID, "name":this.bName, "hour":this.bHour, "capacity":this.bCapacity, "start":this.bStart, "end":this.bEnd});
    this.bName = "";
    this.bHour = null;
    this.bCapacity = null;
    this.bStart = "";
    this.bEnd = "";
  }

}

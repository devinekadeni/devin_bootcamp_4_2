import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CourseListServiceService } from './course-list-service.service';

import { AppComponent } from './app.component';
import { CourseListComponent } from './course-list/course-list.component';

@NgModule({
  declarations: [
    AppComponent,
    CourseListComponent
  ],
  imports: [
    BrowserModule,FormsModule
  ],
  providers: [CourseListServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
